FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY build/libs/multi-module-1.0.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/multi-module-1.0.jar"]
